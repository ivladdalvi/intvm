IntVM is a virtual machine to conduct DevOps / Linux System Engineer
interviews.

Before the interview a candidate needs to provide their ssh public key. They
can login as `root` to start working on the tasks.

Project icon made by [Freepik](http://www.freepik.com) from
[Flaticon](www.flaticon.com) is licensed by [CC 3.0
BY](http://creativecommons.org/licenses/by/3.0/).


# Currently implemented

1. nginx's default site config has a typo (omitted semicolon), and `index.html`
in document root has permissions of 000 with the filesystem being mounted
read-only. Candidate is told "_the webserver at [VM IP address] does not return
a web page. Can you fix it?_" Expected time to completion: _less than 5
minutes_.
2. a daemon has a deleted file opened in `/data`. The fs is full. Candidate is
told "_filesystem /data is full. Could you clean it up?_" Expected time to
completion: _less than 5 minutes)_.

# Interview tasks ideas

Those intended for completion with or without supervision.

1. Unknown service listens on a port and returns "hello world" upon connection.
Make it return "hello there". Candidate is expected to find and modify service
config file and restart service if needed. Possible options: make a custom
daemon service that helds config file open or start it under supevision
((x)inetd, daemontools or with `systemd` socket activation).
2. Firewall misconfiguration (e.g., no state created)
3. Fix `chmod 444 /bin/chmod`
4. find large file, clean up file system (possibly, deleted)
5. Website seem to work, but checks are failing (return 5xx with content)
6. nginx and apache fighting over a TCP socket to listen on
7. images loaded from nfs/gluster/whatever which is not mounted or freezes on
read
8. web application needs to write to a dir but has no permissions
9. typo in `/etc/hosts`
10. something involving `strace` usage to troubleshoot
11. broken ntp

# Interactive questions
- How to check the kernel version of a Linux system? 
- How to see the current IP address on Linux? 
- How to check for free disk space in Linux? 
- How to see if a Linux service is running? 
- How to check the size of a directory in Linux? 
- How to check for open ports in Linux? 
- How to check Linux process information (CPU usage, memory, user information,
  etc.)? 
- How to deal with mounts in Linux 
- Suppose you have a simple web service setup for serving www.example.com, like:
  1. nginx node as LB (load balancer)
  2. php-fpm nodes as application servers
  3. mysql db servers – srvdb1 as master, srvdb2 as slave OS – Linux, any
  distro you like.
    - How would you organize monitoring and alerting?
    - Which params you think you need to monitor?
    - Which tools would you suggest to use for this?
    - Propose a solution which would alert monitoring team if average response
      time for PHP-served page www.example.com/checkout-cart is greater than
      500ms?
    - Any actions required to make such system highly available?

# Links
- https://www.reddit.com/r/devops/comments/8si373/devops_interview_questionscode_tests/
- https://www.youtube.com/watch?v=l0QGLMwR-lY
- https://medium.com/netflix-techblog/linux-performance-analysis-in-60-000-milliseconds-accc10403c55
- https://github.com/chassing/linux-sysadmin-interview-questions
- https://github.com/spikenode/DevOps-Interview-Questions
- https://github.com/jakshi/devops-interview-questions
