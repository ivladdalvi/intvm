#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <unistd.h>

int main() {
  FILE *data;

  openlog("responder",  LOG_PID | LOG_CONS, LOG_LOCAL5);
  syslog(LOG_INFO, "Starting responder daemon.");
  data = fopen("/data/values.dat", "r+");
  if (data == NULL) {
    syslog(LOG_CRIT, "Unable to open data file. Exiting!");
    exit(1);
  }
  remove("/data/values.dat");
  syslog(LOG_INFO, "File opened and removed.");
  while (1) {
    sleep(1);
  }
}
