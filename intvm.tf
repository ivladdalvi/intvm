variable "do_token" {}
variable "ssh_fingerprints" {
  type = "list"
}

provider "digitalocean" {
  token = "${var.do_token}"
}

resource "digitalocean_droplet" "intvm" {
  image  = "ubuntu-18-04-x64"
  name   = "interview"
  region = "sgp1"
  size   = "s-1vcpu-1gb"
  ssh_keys = "${var.ssh_fingerprints}"
  volume_ids = [
    "${digitalocean_volume.www-html.id}",
    "${digitalocean_volume.data.id}"
  ]

  provisioner "remote-exec" {
    script = "scripts/p001"
  }

  provisioner "file" {
    source = "files/default"
    destination = "/etc/nginx/sites-available/default"
  }

  provisioner "file" {
    source = "files/index.html"
    destination = "/var/www/html/index.html"
  }

  provisioner "remote-exec" {
    script = "scripts/p002"
  }

  provisioner "file" {
    source = "files/responder.c"
    destination = "/tmp/responder.c"
  }

  provisioner "file" {
    source = "files/99-responder.conf"
    destination = "/etc/rsyslog.d/99-responder.conf"
  }

  provisioner "file" {
    source = "files/responder.service"
    destination = "/etc/systemd/system/responder.service"
  }

  provisioner "remote-exec" {
    script = "scripts/prepare-data"
  }
}

resource "digitalocean_volume" "www-html" {
  region = "sgp1"
  name = "www-html"
  size = 1
}

resource "digitalocean_volume" "data" {
  region = "sgp1"
  name = "data"
  size = 1
}
